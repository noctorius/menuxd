$(document).ready(function () {
    var headerLinks = $('.header-center__link');

    for (var i = 0; i < headerLinks.length; i++) {
        headerLinks.eq(i).attr('data-aos-delay', (i + 1) * 100);
    }

    //anomation js lib init
    setTimeout(function(){
        AOS.init();
    }, 0);

    //tilt.js lib init
    $('.js-tilt').tilt({
        reset: true
    })

    const background = () => {

        const backgrounds = document.getElementsByClassName('waves-flow');

        const circles = (background) => {
            let mesh;
            let count = 0;
            let originalVertices = [];

            let textureWidth = null;
            let textureHeight = null;
            const backgroundWidth = background.offsetWidth;
            const backgroundHeight = background.offsetHeight;

            const stage = new PIXI.Container();
            const container = new PIXI.Container();
            const texture = new PIXI.Texture.fromImage('imgs/waves-flow/waves-flow.png');
            const app = PIXI.autoDetectRenderer(background.offsetWidth, background.offsetHeight, { transparent: true });

            background.appendChild(app.view);
            stage.addChild(container);

            const cover = (...values) => {
                const calculateSize = positioning(values[0], values[1], values[2], values[3]);

                mesh.width = calculateSize.width + 50;
                mesh.height = calculateSize.height + 30;
                mesh.position.y = calculateSize.offsetTop - 15;
                mesh.position.x = calculateSize.offsetLeft - 15;
            };

            texture.on('update', function () {
                textureWidth = texture.width;
                textureHeight = texture.height;
                mesh = new PIXI.mesh.Plane(this, 20, 20);

                cover(backgroundWidth, backgroundHeight, textureWidth, textureHeight);

                container.addChild(mesh);
                originalVertices = mesh.vertices.slice(0);

                resize();
                animate();
            });

            const animate = () => {
                requestAnimationFrame(animate);
                count += 0.025;

                if (mesh && mesh.vertices) {
                    for (let i = 0; i < mesh.vertices.length; i++) {
                        mesh.vertices[i] = originalVertices[i] + (10 * Math.cos(count + i * 0.15));
                    }
                }

                app.render(container);
            };

            const resize = () => {
                window.addEventListener('resize', () => {
                    const newWidth = background.offsetWidth;
                    const newHeight = background.offsetHeight;

                    cover(newWidth, newHeight, textureWidth, textureHeight);

                    app.view.style.width = newWidth + 'px';
                    app.view.style.height = newHeight + 'px';
                    app.resize(newWidth, newHeight);
                });
            };
        };

        for (let i = 0; i < backgrounds.length; i++) {
            circles(backgrounds[i]);
        }

    };
});